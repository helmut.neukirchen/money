# A Demo for CI

Pipeline status is
<a href="https://gitlab.com/helmut.neukirchen/money/commits/master"><img alt="pipeline status" src="https://gitlab.com/helmut.neukirchen/money/badges/master/pipeline.svg" /></a>

Obtained coverage is
<a href="https://gitlab.com/helmut.neukirchen/money/commits/master"><img alt="coverage report" src="https://gitlab.com/helmut.neukirchen/money/badges/master/coverage.svg" /></a>